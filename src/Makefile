# Copyright (C) 2023  Santiago Previotto
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# \file Makefile
# files macros #
LIB_BLINK_FILE = blink

# level macros #
LIB_COMMON_LEVEL = lib/common

# cpplint macros #
LINT_FLAGS = --recursive --quiet

docker:
ifndef YOUARECONTAINERIZEDBUDDY
	../tools/docker -p $(PWD)/..
else
	echo "The docker image is now available on the system!"
endif
	$(info )
	$(info Welcome to the docker image for programming AVR microcontrollers.)
	$(info )

build:
	cd app/ && avr-gcc -Wall -Os -DF_CPU=$(CPU_FREQ) -mmcu=$(CPU_ID) -c main.c -o main.o
	cd $(LIB_COMMON_LEVEL)/src/ && avr-gcc -Wall -Os -DF_CPU=$(CPU_FREQ) -mmcu=$(CPU_ID) -c $(LIB_BLINK_FILE).c -o $(LIB_BLINK_FILE).o
	cd $(LIB_COMMON_LEVEL)/src/ && mv $(LIB_BLINK_FILE).o ../../../app/	
	cd app/ && avr-gcc -Wall -Os -DF_CPU=$(CPU_FREQ) -mmcu=$(CPU_ID) -o main.elf main.o $(LIB_BLINK_FILE).o
	cd app/ && avr-objcopy -j .text -j .data -O ihex main.elf main.hex
	cd app/ && avr-size --format=avr --mcu=$(CPU_ID) main.elf

erase:
	avrdude -v -p $(CPU_ID) -c $(PROG_ID) -P $(PROG_PORT) -B 3 -e

fuses:
	avrdude -v -p $(CPU_ID) -c $(PROG_ID) -P $(PROG_PORT) -B 3 -U efuse:w:0xff:m -U hfuse:w:0xd9:m -U lfuse:w:0xe2:m -U lock:w:0xff:m
	
flash:
	cd app/ && avrdude -v -p $(CPU_ID) -c $(PROG_ID) -P $(PROG_PORT) -B 3 -U flash:w:main.hex:i -D

lint: 
	cpplint $(LINT_FLAGS) *

burn: erase fuses flash

clean: 
	cd app/ && rm main.hex main.elf main.o $(LIB_BLINK_FILE).o
